import socket
import struct
import threading
import logging


class HexInterface:
  
  def __init__(self, hex_ip):
    self._hex_ip = hex_ip
    
    self.connected = False
    
    self.fx = 0.0
    self.fy = 0.0
    self.fz = 0.0
    self.tx = 0.0
    self.ty = 0.0
    self.tz = 0.0
    
    self._cpf = 1
    self._cpt = 1
    self._scale = [0, 0, 0, 0, 0, 0]
    
    
  def __del__(self):
    self.connected = False
    self.data_thread.join()
    self.disconnect()
    
  
  def connect(self):
    self._data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self._data_socket.connect((self._hex_ip, 49151))
    
    self.connected = True
    
    self.get_scaling()
    
    self.data_thread = threading.Thread(target=self.data_thread)
    self.data_thread.daemon = True
    self.data_thread.start()
    
      
  def disconnect(self):
    self._data_socket.close()
    
  
  def get_scaling(self):
    cmd = "\x01" + "\x00" * 19
    self._data_socket.send(bytes(cmd, 'utf8'))
    msg = self._data_socket.recv(24)
    self.handle_scaling(msg)
  
  
  def data_thread(self):
    while self.connected:
      cmd = "\x00" * 20
      self._data_socket.send(bytes(cmd, 'utf8'))
      msg = self._data_socket.recv(16)
      self.handle_data(msg)
  
  
  def handle_data(self, msg):
    raw_fx = struct.unpack(">h", msg[4:6])[0]
    raw_fy = struct.unpack(">h", msg[6:8])[0]
    raw_fz = struct.unpack(">h", msg[8:10])[0]
    raw_tx = struct.unpack(">h", msg[10:12])[0]
    raw_ty = struct.unpack(">h", msg[12:14])[0]
    raw_tz = struct.unpack(">h", msg[14:16])[0]
    
    self.fx = 1.0 * raw_fx * self._scale[0] / self._cpf
    self.fy = 1.0 * raw_fy * self._scale[1] / self._cpf
    self.fz = 1.0 * raw_fz * self._scale[2] / self._cpf
    self.tx = 1.0 * raw_tx * self._scale[3] / self._cpt
    self.ty = 1.0 * raw_ty * self._scale[4] / self._cpt
    self.tz = 1.0 * raw_tz * self._scale[5] / self._cpt
    
    
  def handle_scaling(self, msg):
    self._cpf = struct.unpack(">I", msg[4:8])[0]
    self._cpt = struct.unpack(">I", msg[8:12])[0]
    self._scale[0] = struct.unpack(">H", msg[12:14])[0]
    self._scale[1] = struct.unpack(">H", msg[14:16])[0]
    self._scale[2] = struct.unpack(">H", msg[16:18])[0]
    self._scale[3] = struct.unpack(">H", msg[18:20])[0]
    self._scale[4] = struct.unpack(">H", msg[20:22])[0]
    self._scale[5] = struct.unpack(">H", msg[22:24])[0]


if __name__ == "__main__":
  pass
